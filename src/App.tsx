import React from 'react';
import './App.css';
import {TableColumnConfig, TableConfig} from "./TableComponent/TableConfig";

import TableComponent from './TableComponent/TableComponent'
import EntityDisplay from "./EntityDisplay/EntityDisplay";

function App() {
    const config = new TableConfig([
        new TableColumnConfig('name', 'Name'),
        new TableColumnConfig('email', 'Email'),
    ], (item, index) => index)

    const items: any[] = [
        {name: 'Name1', email: 'email1'},
        {name: 'Name2', email: 'email2'},
    ]

  return (
    <div className="App">
        <EntityDisplay/>

        {/*<TableComponent config={config} items={items}/>*/}
    </div>
  );
}

export default App;

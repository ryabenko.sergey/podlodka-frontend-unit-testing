import TableComponent from "../TableComponent/TableComponent";
import {TableColumnConfig, TableConfig} from "../TableComponent/TableConfig";


export const useData = () => {
    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(true)

    const requestData = async () => {
        setLoading(true)
        const response = await fetch('data.json');
        const jsonResponse = await response.json()
        setData(jsonResponse.data)
        setLoading(false)
    }

    useEffect(() => {
        requestData()
    }, [])

    export default function EntityDisplay () {
        const { data, loading, refetch } = useData()

        const config = new TableConfig([
            new TableColumnConfig('name', 'Name'),
            new TableColumnConfig('email', 'Name'),
            new TableColumnConfig('date', 'Date'),
        ], (data, index) => `class-${data.id}`)

        const items = data.data.map(item => ({
            name: `${item.first_name} ${item.last_name}`,
            email: item.email,
            date: item.date,
        }))

        const getClassName = (item) => {
            const diff = moment(item.date).diff(moment(), 'days')

            if (diff < 1) {
                return 'table-danger'
            }

            if (diff < 3) {
                return 'table-warning'
            }

            if (diff > 30) {
                return 'table-success'
            }

            return ''
        }

        return (
            <div>
                <button onClick={refetch}>Refresh</button>

                <table>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact date</th>
                    </tr>
                    </thead>
                    <tbody>
                    {items.map(item => (
                        <tr key={item.id} className={getClassName(item)}>
                            <td>{item.name}</td>
                            <td>{item.email}</td>
                            <td>{item.date}</td>
                        </tr>
                    )}
                    </tbody>
                </table>
            </div>
        )
    }
    return {
        data,
        loading,
        refetch: () => requestData()
    }

}

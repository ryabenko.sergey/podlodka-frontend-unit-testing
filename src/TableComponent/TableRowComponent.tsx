import {TableConfig} from "./TableConfig";

interface TableRowProps {
    config: TableConfig
    item: any
    index?: number|undefined
}

const TableRowComponent = ({config, item, index}: TableRowProps) => {
    return (
        <tr data-testid='row' className={config.rowClassResolver(item, index)}>
            {config.columns.map(col => (
                <td data-testid={`col_${col.key}_${item.id}`} key={`col_${col.key}_${item.id}`}>
                    {item[col.key]}
                </td>
            ))}
        </tr>
    )
};

export default TableRowComponent
import { render, cleanup } from '@testing-library/react'
import * as React from 'react';

import TableComponent from "./TableComponent";
import {TableConfig, TableColumnConfig} from "./TableConfig";

jest.mock('./TableRowComponent', () => {
    return function({item, config}) {
        return <tr data-testid='row' data-testid1={`row-${item.id}`} className={config.rowClassResolver(item, 0)}><td>{item.id}</td></tr>
    }
})
const renderComponent = (config, items) =>
    render(<TableComponent config={config} items={items} />)

describe('TableComponent', () => {
    afterEach(cleanup)

    const config = new TableConfig([
        new TableColumnConfig('field', 'Field'),
    ], (data, index) => `class-${data.id}`)

    describe('check proper data render', () => {
        it('contains proper values', () => {
            const { getByTestId, queryAllByTestId } = renderComponent(config, [
                {id: 'some-id-1'},
                {id: 'some-id-2'},
                {id: 'some-id-3'},
                {id: 'some-id-4'},
                {id: 'some-id-5'},
            ])

            expect(queryAllByTestId('row')).toHaveLength(5)
            expect(queryAllByTestId('row')[0]).toHaveClass('class-some-id-1')
            expect(queryAllByTestId('row')[0]).toHaveTextContent('some-id-1')
        })
    })

})
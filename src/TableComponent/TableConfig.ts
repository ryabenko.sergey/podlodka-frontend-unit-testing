export class TableColumnConfig {
    key: string
    title: string

    constructor(key: string, title: string) {
        this.key = key;
        this.title = title;
    }
}

export class TableConfig {
    columns: TableColumnConfig[] = []
    rowClassResolver: (arg: any, index?: number) => any

    constructor(colums: TableColumnConfig[] = [], rowClassResolver: (item: any, index?: number) => any = () => undefined) {
        this.columns = colums;
        this.rowClassResolver = rowClassResolver;
    }
}
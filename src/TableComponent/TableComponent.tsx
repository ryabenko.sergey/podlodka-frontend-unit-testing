import { TableConfig } from './TableConfig'
import TableRowComponent from './TableRowComponent'

interface TableProps {
    config: TableConfig
    items: any[]
}

const TableComponent = ({config, items}: TableProps) => {
    return (
        <table>
            <thead>
            <tr>
                {config.columns.map(col => (
                    <th
                        key={col.key}
                        data-testid='col'>
                        {col.title}
                    </th>
                ))}
            </tr>
            </thead>
            <tbody>
            {items.map((item, index) => <TableRowComponent key={item.id} config={config} item={item} index={index}/>)}
            </tbody>
        </table>
    )
};

export default TableComponent
import { render } from '@testing-library/react'

import TableRowComponent from './TableRowComponent'
import {TableConfig, TableColumnConfig} from "./TableConfig";

const renderComponent = (config, data, index=0) =>
    render(<table><tbody><TableRowComponent item={data} config={config} index={index} /></tbody></table>)

describe('Row', () => {
    it('renders table row with data', () => {
        const config = new TableConfig([
            new TableColumnConfig('id', 'id'),
            new TableColumnConfig('first_name', 'first_name'),
            new TableColumnConfig('last_name', 'last_name'),
            new TableColumnConfig('date', 'date'),
        ], (item, index) => `row-${item.id}-${index}`)

        const { getByTestId } = renderComponent(config, {
            id: 'foo',
            first_name: 'first',
            last_name: 'last',
            date: 'test'
        })

        expect(getByTestId('row')).toBeInTheDocument()
        expect(getByTestId('row')).toHaveClass('row-foo-0')
        expect(getByTestId('col_id_foo')).toHaveTextContent('foo')
        expect(getByTestId('col_first_name_foo')).toHaveTextContent('first')
        expect(getByTestId('col_last_name_foo')).toHaveTextContent('last')
        expect(getByTestId('col_date_foo')).toHaveTextContent('test')
    })
})
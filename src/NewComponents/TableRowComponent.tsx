import TableConfig from "./TableConfig";

interface TableRowProps {
    config: TableConfig
    items: any[]
}

const TableRowComponent = ({config, item}: TableRowProps) => {
    return (
        <tr data-testid="row" className={config.classResolver(item)}>
            {config.columns.map(col => (
            <td key={col.key} data-testid={col.key}>{item[col.key]}</td>
            ))}
        </tr>
    )
}

export default TableRowComponent
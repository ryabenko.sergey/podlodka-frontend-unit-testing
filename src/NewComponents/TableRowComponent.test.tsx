import { render } from '@testing-library/react'

import TableRowComponent from './TableRowComponent'
import TableConfig, {TableColumnConfig} from "./TableConfig";

function createComponent(config: TableConfig, item: { name: string; id: string }) {
    return render(<table><tbody><TableRowComponent config={config} item={item}/></tbody></table>);
}

describe('Row', () => {
    it('renders table row with all columns', () => {
        const config = new TableConfig([
            new TableColumnConfig("id", "Id"),
            new TableColumnConfig("name", "Name"),
        ])

        const item = {id: 'some-id', name: 'some-name'}

        const {getByTestId} = createComponent(config, item)

        expect(getByTestId('row').querySelectorAll('td')).toHaveLength(2)
        expect(getByTestId('id')).toHaveTextContent('some-id')
        expect(getByTestId('name')).toHaveTextContent('some-name')
    })

    it('renders table row with class name', () => {
        const config = new TableConfig([
            new TableColumnConfig("id", "Id"),
        ], (item) => `class-${item.id}`)

        const item = {id: 'some-id'}

        const {getByTestId} = createComponent(config, item)

        expect(getByTestId('row')).toHaveClass('class-some-id')
    })
})